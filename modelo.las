\contentsline {sig}{\hbox to\@tempdima {RLMS\hfil }{Remote Lab Management System}}{23}
\contentsline {sig}{\hbox to\@tempdima {GT-MRE\hfil }{Grupo de Trabalho em Experimenta\c c\~ao Remota M\'ovel}}{23}
\contentsline {sig}{\hbox to\@tempdima {RExLab\hfil }{Remote Experimentation Laboratory}}{23}
\contentsline {sig}{\hbox to\@tempdima {LM\hfil }{Laborat\'orio M\'ovel}}{24}
\contentsline {sig}{\hbox to\@tempdima {IoT\hfil }{Internet of Things}}{24}
\contentsline {sig}{\hbox to\@tempdima {M-Learning\hfil }{Mobile Learning}}{29}
\contentsline {sig}{\hbox to\@tempdima {MRE\hfil }{Mobile Remote Experimentation}}{29}
\contentsline {sig}{\hbox to\@tempdima {MIT\hfil }{Massachusetts Institute of Technology}}{38}
\contentsline {sig}{\hbox to\@tempdima {ISA\hfil }{iLab Shared Architecture}}{38}
\contentsline {sig}{\hbox to\@tempdima {ISB\hfil }{Interactive Service Broker}}{38}
\contentsline {sig}{\hbox to\@tempdima {ITS\hfil }{Interactive Lab Server}}{39}
\contentsline {sig}{\hbox to\@tempdima {RELLE\hfil }{Remote Lab Learning Environment}}{39}
\contentsline {sig}{\hbox to\@tempdima {AVA\hfil }{Ambientes Virtuais de Aprendizagem}}{39}
\contentsline {sig}{\hbox to\@tempdima {LIS\hfil }{RELLE - Labs Instances Scheduling}}{41}
\contentsline {sig}{\hbox to\@tempdima {PAC\hfil }{Placa de Aquisi\c c\~ao e Controle}}{41}
\contentsline {sig}{\hbox to\@tempdima {API\hfil }{Application Programming Interfaces}}{49}
\contentsline {sig}{\hbox to\@tempdima {SBC\hfil }{\textit {Single Board Computer}}}{54}
